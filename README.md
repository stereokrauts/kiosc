# Kiosc.js #

**An Open Sound Control (OSC) implementation written in javascript.**

Kiosc tries to use [Typed Arrays] https://developer.mozilla.org/en-US/docs/Web/API/DataView) to handle OSC strings which should be fast and efficient enough to build a realtime app.

**Note:** The project is currently a proof of concept to evaluate if a robust implementation of a binary protocol is possible in javascript. It makes use of typed arrays which are fairly new and may not work in all browsers:
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Typed_arrays

