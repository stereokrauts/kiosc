/**
 * Kiosc: An implementation of the OSC specification V1.0.
 * This is not intended to be a full blown OSC
 * all-in-one solution. It's a slim OSC
 * library that can be used to build powerful
 * HTML5 OSC server/client apps. 
 * 
 * The base class of the kiosc.js project
 * (C) 2014, Stereokrauts GbR
 *
 * Add licensing infos here.

 * @author Roland Jansen, jansen@stereokrauts.com
 */

//namespace
var Kiosc = Kiosc || {};

//node compatibility
if (typeof module !== 'undefined' 
	&& module.exports !== 'undefined') {
    module.exports = Kiosc;
}

Kiosc.version = '0.0.1';

var getType = {}.toString;
var buffer = ArrayBuffer(16);
console.log(getType.call(buffer));
var view = Uint8Array(buffer);
console.log(getType.call(view));


