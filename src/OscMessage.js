/**
 * An OSC message data model
 * 
 * @author Roland Jansen, jansen@stereokrauts.com
 */

/**
  * @module Kiosc
  */

// namespace:
this.Kiosc = this.Kiosc || {};

(function() {
	'use strict';

	var OscMessage = function(addressOrBinary, values) {
 		this.initialize(addressOrBinary, values);
 	};
 	var p = OscMessage.prototype;

//constructor
	/**
	 *
	 * @param {String, blob} addr An OSC address pattern in the form "/a/b/c"
	 * @param {Array} args OSC Arguments. Every argument itself is an array with
	 *       two entries: ['data-type', value]. Datatypes can be
	 * - int
	 * - float
	 * - string
	 * - boolean
	 */
 	p.initialize = function(addressOrBinary, values) {
 		if (arguments[0]) {
 			var type = this._getType.call(arguments[0]);
 			
 			if (type === '[object ArrayBuffer]') {
 				console.log('ein binary');
 				this.decodeBinary(arguments[0]);
 			} else if (type === '[object String]') {
 				console.log('ein string');
 				this.setAddress(arguments[0]);
 			} else {
 				//fail silently (could be intended by the user).
 			}

 		}

 		if (arguments[1]) {
 			this.typeTag = this.setTypeTags(arguments[1]);
 			this.argumentList = this.setArguments(arguments[1]);
 		}

 	};

//public properties
	/**
	 *
	 */
	p.utils = new Kiosc.Utils();
	
	/**
 	 * @property {Uint8Array} address A byte array that stores the address pattern
 	 */
 	p.address = null;

 	/**
 	 *
 	 */
 	p.typeTags = null;

 	/**
 	 *
 	 */
 	p.typeTagArray = null;

 	/**
 	 *
 	 */
 	p.argumentList = null;

 //private properties
 	/**
 	 * @property {Object} _getType A pseudo-property. It's a reference to the "toString" method of the passed argument. Like this: "this._getType.call(obj)".
 	 * @private
 	 */
 	p._getType = {}.toString;


//public functions
	
	/**
	 *
	 */
	p.encodeArguments = function(argv, typeTag) {

	};

	/**
	 * Checks if a given string is a valid
	 * OSC address or not
	 * @private
	 * @param {String} addr the string to be checked
	 * @return {Boolean} true if valid, false if not.
	 */
	p.validateAddress = function(addr) {
		return (addr.charAt(0) === '/') ? true : false;
	};

	/**
	 *
	 * @param {Array} types one or more data types for values assotiated with this message
	 * @return {Boolean} true if valid, false if not.
	 */
	p.validateDataTypes = function(types) {
		var val = false;
		for (var i = 0; i < types.length; i++) {
			var t = types[i].charAt(0);
			if (t === 'i' || t === 'f' || t === 's' || t === 'b') {
				val = true;
			} else {
				console.log(types[i] + ' is not a valid OSC-msg argument type.');
				return false;
			}
		}
		return val;
	};

	/**
 	 * Set the binary OSC address from a string pattern
 	 * @param {String} addr The address pattern
 	 * @return {Boolean} true if it succeeds, false if it fails.
 	 */
	p.setAddress = function(addr) {
		if (this.validateAddress(addr)) {
			this.address = this.utils.encodeOscString(addr);
			return true;	
		} else {
			return false;
		}
	};

	/**
	 * Get the OSC address as string
	 * @return {String} The OSC address value
	 */
	p.getAddress = function() {
		return this.utils.decodeOscString(this.address);
	};

	p.setTypeTags = function(typeArray) {
		var typeStr = ',';
		if (this.validateDataTypes(typeArray)) {
			this.typeTagArray.length = 0;
			this.typeTags.length = 0;
			for (var i = 0; i < typeArray.length; i++) {
				var char = typeArray[i].charAt(0);
				this.typeTagArray[i] = char;
				typeStr += char;
			}
			this.typeTags = this.util.encodeOscString(typeStr);
			return true;
		} else {
			return false;
		}
	};

	p.getTypeTags = function() {
		var tags = this.utils.decodeOscString(this.typeTags);
		return tags.substr(1);
	};

	p.setArguments = function(argv) {

	};

	p.getArguments = function() {

	}

	p.addArgument = function(arg) {

	};

	p.resetArguments = function() {
		this.argumentList = null;
	};


	Kiosc.OscMessage = OscMessage;
}());
