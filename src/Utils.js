/**
 * Utility functions usefull for working with
 * OSC messages and bundles.
 * 
 * @author Roland Jansen, jansen@stereokrauts.com
 */

 /**
  * @module Kiosc
  */

// namespace:
this.Kiosc = this.Kiosc || {};

 (function() {
 	'use strict';

 	var Utils = function() {
 		this.initialize();
 	};
 	var p = Utils.prototype;

 //constructor
 	p.initialize = function() {
 		return this;
 	};

 //public properties

 	/**
 	 *
 	 */
 	p.typeTags = ['i', 'f', 's', 'b'];

 	/**
 	 *
 	 */
 	p.typeTagsNonStandard = ['h', 't', 'd', 'S', 'c', 'r',
 			'm' , 'T', 'F', 'N', 'I', '[', ']'];

 	/**
 	 * Takes an ascii coded string literal
 	 * and returns a byte array that matches
 	 * the OSC 1.0 spec for OSC-strings:
 	 * All OSC datatypes must be a multiple of 32bits.
 	 *
 	 * @param {String} ascii the string to be encoded
 	 * @return {Uint8Array} the encoded OSC string
 	 */
 	p.encodeOscString = function(ascii) {
 		var buffer; // array buffer for binary data
 		var aLength = ascii.length + 1;
 		var remainder = aLength % 4;

 		if (remainder !== 0) {
 			buffer = new ArrayBuffer(aLength + 4 - remainder);
 		} else {
 			buffer = new ArrayBuffer(aLength);
 		}
 		
 		var oscStr = new Uint8Array(buffer);

 		for (var i = 0; i < ascii.length; i++) {
 			var charCode = ascii.charCodeAt(i);
 			if (0 < charCode < 128) {
 				oscStr[i] = charCode;
 			} else {
 				var msg = ascii + ': ' + ascii.charAt(i) + 'is not a valid char. Aborting.';
 				alert(msg);
 				throw new Error(msg);
 			}
		}
		return oscStr;
 	};

 	/**
 	 * 
 	 */
 	p.decodeOscString = function(binary) {
 		return String.fromCharCode.apply(null, binary);
 	};

 	p.getBlobFromArrayBuffer = function(buffer) {
 		//probably not needed if we can send the buffer-content
 		//directly to the bus.
 		return new Blob([buffer]);
 	};




 	Kiosc.Utils = Utils;
 }());