/**
 * Tests for the Kiosc.js class
 * @author Roland Jansen, jansen@stereokrauts.com
 */
describe('A Kiosc class', function() {

	it('should define its namespace', function() {
		expect(window.Kiosc).toBeDefined();
		expect(Kiosc).toBeDefined();
	});

	
});