/**
 * Helper functions for the test suite
 *
 * @author Roland Jansen, jansen@stereokrauts.com
 */
var customMatchers = {

	toBeUint8Array: function(util, customEqualityTesters) {
		return {
			compare: function(actual, expected) {
				var result = {};
				var toClass = {}.toString;
				expected = '[object Uint8Array]'
				
				result.pass = util.equals(toClass.call(actual),
					expected, customEqualityTesters);
				return result;
			}
		};
	}
};