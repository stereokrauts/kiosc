/**
 * Tests for the OscMessage class
 * It needs a modern environment
 * as it uses typed arrays.
 * @author Roland Jansen, jansen@stereokrauts.com
 */
describe('A OscMessage class', function() {
	var oscMsg;

	beforeEach(function() {
		jasmine.addMatchers(customMatchers);
		oscMsg = new Kiosc.OscMessage();
	});

	afterEach(function() {
		oscMsg = null;
	});

/*
	it('should fail if the browser doesn\'t support Typed Arrays', function() {
		oscMsg.useTypedArrays = false;
		(function() { oscMsg.create(); })
		.toThrow(new Error('Your browser doesn\'t support Typed Arrays. '
			+ 'Please upgrade to a modern browser.'));
	});
*/

	it('should initialize properly with a binary message', function() {
		// addr: "/oscillator/4/frequency"
		// type: "f" (float)
		// value: 440.0
		var binMsg = new Uint8Array([
			0x2f, 0x6f, 0x73, 0x63, 0x69, 0x6c, 0x6c, 0x61,
			0x74, 0x6f, 0x72, 0x2f, 0x34, 0x2f, 0x66, 0x72,
			0x65, 0x71, 0x75, 0x65, 0x6e, 0x63, 0x79, 0x00,
			0x2c, 0x66, 0x00, 0x00, 0x43, 0xdc, 0x00, 0x00 ]);

		var msg = new Kiosc.OscMessage(binMsg.buffer);
		//expect(msg.getAddress()).toMatch('/oscillator/4/frequency');
		//expect(msg.getTypeTags()).toMatch('f');
		//expect(msg.getArguments(1)).toEqual(440.0);
	});

	it('should initialize properly with a binary message with 5 different values)', function() {
		// addr: "/foo"
		// type: "iisff"
		// values: "1000", "-1", "hello", "1.234", "5.678"
		var binMsg = new Uint8Array([
			0x2f, 0x66, 0x6f, 0x6f, 0x00, 0x00, 0x00, 0x00,
			0x2c, 0x69, 0x69, 0x73, 0x66, 0x66, 0x00, 0x00,
			0x00, 0x00, 0x03, 0xe8, 0xff, 0xff, 0xff, 0xff,
			0x68, 0x65, 0x6c, 0x6c, 0x6f, 0x00, 0x00, 0x00,
			0x3f, 0x9d, 0xf3, 0xb6, 0x40, 0xb5, 0xb2, 0x2d ]);

	})

	it('should initialize properly with an OSC address (string)', function() {
		var msg = new Kiosc.OscMessage('/foo/bar');
	});

	it('should initialize properly with a one or more values', function() {

	});

/********************************
	it('should validate a given address pattern', function() {
		var result1 = oscMsg.setAddress("++$/asdf");
		var result2 = oscMsg.setAddress("/   /.=?/asdf");
		expect(result1).toBeFalsy();
		expect(result2).toBeTruthy();
	});

	it('should encode an address string to OSC address', function() {
		oscMsg.setAddress('/abc/123/$%&');
		expect(oscMsg.address).toBeUint8Array();
		expect(oscMsg.address.length).toEqual(16);
	});

	it('should return the OSC address as string', function() {
		oscMsg.setAddress('/abc/123');
		expect(oscMsg.getAddress()).toMatch('/abc/123');
	});

	it('should take an integer as argument and set the type-string accordingly', function() {
		oscMsg.addArgument(['int', 123]);
		expect(oscMsg.getTypeTags()).toMatch('i');
	});
***************************************/

/*
	it('should encode its address as a Typed Array', function() {
		oscMsg.setAddressPattern("/new/pattern");
		var result = oscMsg.getAddressPattern();
		expect(result).toBeUint8Array();
	});

	it('should have an address length of n*4 bytes', function() {
		oscMsg.setAddressPattern("/new/pattern");
		var result = oscMsg.getAddressPattern();
		expect(result.length).toEqual(16);
		// another test for 15bytes
		oscMsg.setAddressPattern("/new/pattern/ab");
		var result = oscMsg.getAddressPattern();
		expect(result.length).toEqual(16);
	});

*/
/*
	it('should validate a given data type string', function() {

	});

	it('should encode a data type string into an OSC Type Tag', function() {

	});

	it('should have an OSC Type Tag length of n*4 bytes', function() {

	});
*/
});