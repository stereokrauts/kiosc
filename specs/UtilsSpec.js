/**
 * Tests for the Utils class
 * It needs a modern environment
 * as it uses typed arrays.

 * @author Roland Jansen, jansen@stereokrauts.com
 */
 describe('A Utils class', function() {
 	var utils;

 	beforeEach(function() {
 		//jasmine.addMatchers(customMatchers);
 		utils = new Kiosc.Utils();
 	});

 	afterEach(function() {
 		utils = null;
 	});


 	it('should define the OSC type tags', function() {
 		expect(utils.typeTags.indexOf('i')).toBeGreaterThan(-1);
 		expect(utils.typeTags.indexOf('f')).toBeGreaterThan(-1);
 		expect(utils.typeTags.indexOf('s')).toBeGreaterThan(-1);
 		expect(utils.typeTags.indexOf('b')).toBeGreaterThan(-1);
 	});

 	it('should encode an ascii string to an OSC binary string', function() {
 		var str = 'ThisStringHas20Chars';
 		var binaryData = utils.encodeOscString(str);
 		expect(binaryData.byteLength).toEqual(24);
 		expect(binaryData[0]).toEqual(84);
 		expect(binaryData[13]).toEqual(50);
 		expect(binaryData[20]).toEqual(0);
 	});

 	it('should decode a binary string to ascii', function() {
 		//var binary = utils.encodeOscString('StrHas13Chars');
 		var binary = new Uint8Array([83, 116, 114, 72, 97, 115, 49, 51, 67, 104, 97, 114, 115]);
 		var ascii = utils.decodeOscString(binary);
 		expect(ascii).toMatch('StrHas13Chars');
 	});

 });